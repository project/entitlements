<?php

/**
 * @file
 * Documentation for entitlements API.
 */

/**
 * Implement this hook to expose the title and a short description of your entitlements provider.
 */
function hook_entitlements_provider_info() {
  return array(
    'title' => t('Default provider'),
    'description' => t('Default provider of entitlements.'),
  );
}

/**
 * Implement this hook to expose the title and a short description of your parser.
 *
 * @param $account
 *   (optional) The user object for lookup.
 */
function hook_entitlements_provider($account) {

}

/**
 * Implement this hook to expose the title and a short description of your entitlements consumer.
 */
function hook_entitlements_consumer_info() {
  return array(
    'title' => t('Default consumer'),
    'description' => t('Default consumer of entitlements.'),
  );
}

function hook_entitlements_consumer() {

}
