<?php

/**
 * @file
 * Admin page callback file for the entitlements module.
 */

/**
 * Provide default administrative form
 */
function entitlements_admin_form() {
  $items = array();

  // get all possible, installed providers
  $provider_modules = module_implements('entitlements_provider_info');
  foreach ($provider_modules as $provider) {
    $provider_info = module_invoke($provider, 'entitlements_provider_info');
    $providers[$provider] = $provider_info['title'] . ' (<em>' . $provider_info['description'] . '</em>)';
  }

  $items['entitlements_active_providers'] = array(
    '#title' => t('Active providers'),
    '#type' => 'checkboxes',
    '#description' => t('Select which providers to actively use.'),
    '#options' => $providers,
    '#default_value' => variable_get('entitlements_active_providers', array()),
  );

  $items['performance'] = array(
    '#title' => t('Performance'),
    '#type' => 'fieldset',
  );

  $items['performance']['entitlements_cache_lifetime'] = array(
    '#title' => t('Cache lifetime'),
    '#type' => 'select',
    '#description' => t('The minimum time to cache entitlements to speed lookups.<br/>NOTE: Setting this to "Never" could severely degrade performance if entitlements provider is slow. Use only on development environments for testing.'),
    '#options' => array(
      '-1' => t('Per session'),
      '0' => t('Never'),
      '3600' => t('1 hour'),
      '7200' => t('2 hours'),
      '10800' => t('3 hours'),
      '14400' => t('4 hours'),
      '86400' => t('1 day'),
      '604800' => t('1 week'),
    ),
    '#default_value' => variable_get('entitlements_cache_lifetime', -1),
  );

  $items['performance']['entitlements_cache_in_user_object'] = array(
    '#title' => t('Use user object for caching'),
    '#type' => 'checkbox',
    '#description' => t('Store entitlements in individual user objects, rather than a cache table.'),
    '#default_value' => variable_get('entitlements_cache_in_user_object', FALSE),
  );

  return system_settings_form($items);
}
