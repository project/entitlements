<?php

/**
 * @file
 * Page callback file for the entitlements module.
 */

/**
 * User profile tab listing all entitlements for a given user.
 */
function entitlements_user_profile($account) {
  $build = array();

  $headers = array(t('Provider'), t('Name'), t('Description'));
  $rows = array();

  // get list of entitlements for current user  
  $entitlements = _entitlements_user_load($account);
  foreach ($entitlements as $provider => $list) {
    foreach ($list as $entitlement) {
      $rows[] = array($provider, $entitlement['title'], $entitlement['description']);
    }
  }

  $build['entitlements'] = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#caption' => t('If entitlements are missing, make sure the correct providers are ' . l('set active', 'admin/people/entitlements') . '.'),
    '#empty' => t('No entitlements found.'),
  );

  return $build;
}
